﻿"""
Copyright (c) 2022 Valde & Guto (https://gitlab.com/wpserver)
All rights reserved.
Use of this source code is governed by a BSD-style 3-Clause
license that can be found in the LICENSE file.
"""
import tkinter as tk
import urllib.request as request
import urllib.error as uerror
import pickle
from os import walk, remove
from os.path import exists
from threading import Thread, Lock

from funcs import repk3, relink, error_log, load_path, resource_path


PATH = load_path()
ICONPTH = resource_path("")


class App(tk.Frame):
    def __init__(self, master: tk.Tk = None):
        super().__init__()

        self.master = master
        self.master.title("WP Packages Manager 1.0.0")
        self.master.wm_iconbitmap(f"{ICONPTH}\\wpkm.ico")
        self.master.resizable(False, False)
        self.master.config(padx=2, pady=2)
        self.pack()

        self.lock = Lock()
        self.urls = {}

        self.create_widgets()

    def create_widgets(self):
        label_text = "Click on Download to download all wp packages"
        self.label = tk.Label(self, font=("", 10), text=label_text, width=40)
        self.label.pack(expand=True, fill="both")

        self.fr = tk.Frame(self)
        self.fr.pack(expand=True, fill="both")

        self.down_btn = tk.Button(
            self.fr,
            text="Download",
            command=lambda: Thread(target=self.start_download).start()
            )
        self.down_btn.pack(side="left", expand=True, fill="both")

    def update_urls(self):
        """Downlaod the file pkurls from a direct url"""

        self.label["fg"] = "blue"
        self.label["text"] = "Searching for new packages..."

        try:
            url = "http://wpserver.us.to"
            url = request.Request(url, headers={"User-Agent": "Mozilla/5.0"})
            data = request.urlopen(url)
            with open("pkurls", "wb+") as pkurls:
                pkurls.write(data.read())
                pkurls.seek(0)
                self.urls = pickle.load(pkurls)

            return True
        except uerror.URLError:
            self.label["fg"] = "red"
            self.label["text"] = "Error! Verify your internet connection."
            return False
        except Exception as error:
            self.label["fg"] = "red"
            self.label["text"] = "Unknown error."
            error_log("Update", error.__class__.__name__)
            return False

    def download(self):
        """Download new packages if necessary"""

        pkdown = False
        for pkname, pkurl in self.urls.items():
            if exists(f"{PATH}\\{pkname}"):
                continue

            try:
                self.label["fg"] = "blue"
                self.label["text"] = f"Downloading file {pkname}..."
                url = request.Request(
                    pkurl, headers={"User-Agent": "Mozilla/5.0"}
                    )
                data = request.urlopen(url)
                with open(f"{PATH}\\{pkname}", "wb") as file:
                    file.write(data.read())

                pkdown = True
            except uerror.URLError:
                self.label["fg"] = "red"
                self.label["text"] = "Error! Verify your internet connection."
                return
            except Exception as error:
                self.label["fg"] = "red"
                self.label["text"] = "Unknown error."
                error_log("Download", error.__class__.__name__)
                return

        if not pkdown:
            self.label["fg"] = "blue"
            self.label["text"] = "There's not new packages to download"
        else:
            self.label["fg"] = "green"
            self.label["text"] = "Download finished!"

    def remove_files(self):
        """Remove old/unused packages files"""

        for root, folders, files in walk(PATH):
            for file in files:
                if not root.lower().endswith("main"):
                    continue

                ftype = repk3.search(file)
                if not ftype:
                    continue

                if file in self.urls:
                    continue

                remove(f"{PATH}\\{file}")

    def start_download(self):
        """Function called after click on Download button"""

        if not exists(PATH):
            self.label["fg"] = "red"
            self.label["text"] = "Main folder not found!"
            return

        self.down_btn["state"] = "disabled"
        self.lock.acquire()

        allowed = self.update_urls()
        if allowed:
            self.download()
            self.remove_files()

        self.lock.release()
        self.down_btn["state"] = "normal"


if __name__ == "__main__":
    root = tk.Tk()
    app = App(root)
    app.mainloop()
