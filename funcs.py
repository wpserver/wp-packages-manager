﻿"""
Copyright (c) 2022 Valde & Guto (https://gitlab.com/wpserver)
All rights reserved.
Use of this source code is governed by a BSD-style 3-Clause
license that can be found in the LICENSE file.
"""
import re
import sys
from os import environ, path
from os.path import abspath, join


path = (environ["LOCALAPPDATA"] +
        "\\VirtualStore\\Program Files (x86)\\Call of Duty\\Main"
        )
repk3 = re.compile(r"((?:___wp|wp)(?:.+)(?:\.pk3))", re.I)
relink = re.compile(r"(?:\")(http.+)(?:\")", re.I)


def resource_path(relative_path):
    try:
        base_path = sys._MEIPASS
    except Exception:
        base_path = abspath(".")

    return join(base_path, relative_path)


def load_path():
    global path

    try:
        with open("custom_path.txt") as file:
            txt = file.read()
    except FileNotFoundError:
        with open("custom_path.txt", "w") as file:
            file.write('$path = "";\n')

        return load_path()

    custom = re.findall(r"(?:\$path\s*=\s*\")(.+main\b)(?:\";)", txt, re.I|re.M)
    if custom:
        path = custom[0]

    return path


def error_log(section, error):
    with open("error.log", "a") as log:
        log.write(f"======== {section} ========\n{error}\n\n")


def generate_file():
    """Only use it to generate a pkurls file"""

    import pickle

    packs = {
        "___wpcfgv3.pk3": "https://is.gd/Bf5N53",
        "___wp-map-pack.pk3": "https://is.gd/KlqVuP",
        "___wp-map-pack2.pk3": "https://is.gd/m5IcCC",
        "___wp-map-pack3.pk3": "https://is.gd/ltnDvQ",
        "wp_anticheat_modv4.pk3": "https://is.gd/l4S00k"
        }

    with open("pkurls", "wb") as pkurls:
        pkurls.write(pickle.dumps(maps))


if __name__ == "__main__":
    #generate_file()
    pass
